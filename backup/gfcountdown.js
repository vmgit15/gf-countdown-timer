(function ($) {
    var gfCountdown = function (el, opt, st, cb) {
        this.$el = $(el);
        let self = this;
        this.cb = cb;

        console.log(cb);
        // merge settings with options
        if (opt) {
            this.settings = $.extend(st, opt);
        }
        this.totalCountdownSeconds = ((new Date(this.settings.date)).getTime() - Math.floor(Date.now()));
        console.log(this.totalCountdownSeconds);
        setCookie(finalDate, this.settings.date.toUTCString(), 30);
        // this.handleSettings();
        this.start();
    }

    $.extend(gfCountdown.prototype, {
        callback: function (func) {
            func();
        },

        handleSettings: function () {
            // end the plugin if no date provided
            if (this.settings.date === null) {
                alert("No date input!!");
                return;
            }
            // process control options
            let ctrl = this.settings.control;
            switch (ctrl) {
                case "pause":
                case "stop":
                    this.stop();
                    break;
                case null:
                case "resume":
                    this.start();
                    break;
                default:
                    alert("Unknown control option! The timer will now end!");
                    break; //return;
            }

        },
        start: function () {
            console.log(this.totalCountdownSeconds);
            if (this.interval !== null) {
                clearInterval(this.interval);
            }
            let self = this;

            this.render();
            this.interval = setInterval(function () {
                self.render();
            }, 1000);
        },
        stop: function () {
            clearInterval(this.interval);
            this.interval = null;
        },
        timeOut: function (actionType, actionName) {
            console.log('let\'s do this');
            switch (actionType) {
                case 'alert': // This works but misses 1s
                    this.stop();
                    alert(actionName);
                    break;
                case 'redirect': // This works
                    this.stop();
                    document.location.replace(actionName);
                    break;
                case 'reset': // This doesn't work
                    let newDate = new Date(Math.floor(Date.now()) + this.totalCountdownSeconds);
                    console.log(newDate);
                    this.settings.date;
                    this.$el.countdown({
                        date: newDate,
                        elapse: "no"
                    }, function () {
                        alert("Countdown finished!");
                    });
                    break;
                case 'hide': // This works
                    this.stop();
                    this.$el.hide();
                    break;
                default: // This works
                    this.stop();
                    console.log('This is the end');
                    break;
            }
        },
        render: function () {
            let expDate = (new Date(this.settings.date)) / 1e3;
            let now = Math.floor(Date.now() / 1e3);

            let totalRemainingSec = expDate - now;
            totalRemainingSec = !this.settings.elapse && totalRemainingSec < 0 ? 0 : Math.abs(totalRemainingSec);

            // check if the expiry date has passed
            this.elapsed = (now >= expDate);

            let offset = {
                seconds: totalRemainingSec % 60,
                minutes: Math.floor(totalRemainingSec / 60) % 60,
                hours: Math.floor(totalRemainingSec / 60 / 60) % 24,
                days: Math.floor(totalRemainingSec / 60 / 60 / 24) % 7,
                totalDays: Math.floor(totalRemainingSec / 60 / 60 / 24),
                totalHours: Math.floor(totalRemainingSec / 60 / 60),
                totalMinutes: Math.floor(totalRemainingSec / 60),
                totalSeconds: totalRemainingSec
            }
            console.log(totalRemainingSec);

            if (!isNaN(expDate)) {
                this.$el.html(offset.totalDays + "d " + offset.hours + "h " + offset.minutes + "m " + offset.seconds + "s");
            }
            else {
                alert("Invalid date");
                this.stop();
            }

            // stop the timer when it reaches 0 and elapse mode is off
            if (this.settings.elapse !== "yes" && totalRemainingSec === 0) {
                // this.stop();
                this.callback(this.timeOut('reset', 'https://www.facebook.com/'));
                // return;
            }
        },
        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
    });

    $.fn.countdown = function (options, callback) {
        // list of custom settings
        var settings = {
            date: null,
            elapse: null,
            format: null,
            control: null
        };
        return this.each(function () {
            if ($(this).data("gfcountdown") === undefined) {
                var plugin = new gfCountdown(this, options, settings, callback);
                $(this).data("gfcountdown", plugin);
            }
        });
    }
}(jQuery));