(function ($) {
    var gfCountdown = function (el, st, cb) {
        this.$el = $(el);
        this.cb = cb;
        this.settings = st;
        let self = this;

        // console.log(document.cookie);

        // if no date passed in settings
        if (this.settings.date === null) {
            // read date from cookies
            let cookies = document.cookie.split(";");
            let cdate = cookies.filter(function (item) {
                return item.indexOf("cdate=") !== -1;
            }).join("");
            cdate = cdate.slice(cdate.indexOf("=") + 1, -1);
            // get cdate if found in cookies
            if (cdate !== "") this.settings.date = cdate;
        }

        this.start();
    }

    $.extend(gfCountdown.prototype, {
        callback: function (func) {
            func();
        },

        handler: function () {
            let self = this;

            // prompt to ask user to provide a date
            if (this.settings.date === null) {
                this.$el.text("Please provide a Date and restart the Timer!");
                return;
            }

            // process control options
            switch (this.settings.control) {
                case "pause":
                case "stop":
                    this.stop();
                    break;
                case null:
                case "start":
                case "resume":
                    this.interval = setInterval(function () {
                        self.render();
                    }, 1000);
                    break;
                default:
                    alert("Unknown control option! The timer will now end!");
                    break; //return;
            }
        },

        start: function () {
            clearInterval(this.interval);

            // first time render
            // this.render();

            this.handler();
        },

        stop: function () {
            try { // 5ea1407 error shoots when pass in a past date
                clearInterval(this.$el.data("gfcountdown").interval);
            }
            catch (err){}
            clearInterval(this.interval);
            this.interval = null;
        },

        calcTime: function() {
            let expDate = (new Date(this.settings.date).getTime()) / 1e3;
            let now = Math.floor(Date.now() / 1e3);

            console.log("date: " + this.settings.date);
            console.log("exp date: " + expDate);
            console.log("now: " + now);

            let totalRemainingSec = expDate - now;
            totalRemainingSec = !this.settings.elapse && totalRemainingSec < 0 ? 0 : Math.abs(totalRemainingSec);

            let offset = {
                seconds: totalRemainingSec % 60,
                minutes: Math.floor(totalRemainingSec / 60) % 60,
                hours: Math.floor(totalRemainingSec / 60 / 60) % 24,
                days: Math.floor(totalRemainingSec / 60 / 60 / 24) % 7,
                totalDays: Math.floor(totalRemainingSec / 60 / 60 / 24),
                totalHours: Math.floor(totalRemainingSec / 60 / 60),
                totalMinutes: Math.floor(totalRemainingSec / 60),
                totalSeconds: totalRemainingSec
            }
            console.log("total remaining sec: " + totalRemainingSec);
            console.log("----");

            return offset;
        },

        render: function () {
            let offset = this.calcTime();

            // stop the timer when it reaches 0 and elapse mode is off
            if (!this.settings.elapse && offset.totalSeconds === 0) {
                this.stop();
                this.callback(this.cb);
                // return;
            }

            // render
            if (!isNaN(offset.totalSeconds)) {
                this.$el.html(offset.totalDays + "d " + offset.hours + "h " + offset.minutes + "m " + offset.seconds + "s");
            }
            else {
                this.stop();
                console.log("Invalid date! Timer stopped!");
            }
        }
    });

    $.fn.countdown = function (options, callback) {
        // list of custom settings
        var settings = {
            date: null,
            elapse: null,
            format: null,
            control: null
        };

        // apply NEW options to settings
        if (options) {
            settings = $.extend(settings, options);
        }
        // save date to cookies if a date is passed in
        // for later use (pause case)
        if (settings.date !== null) {
            document.cookie = "cdate=" + settings.date + "; path=/";
        }

        return this.each(function () {
            // if ($(this).data("gfcountdown") === undefined) {
            // console.log($(this).data("gfcountdown"));
            var plugin = new gfCountdown(this, settings, callback);
            $(this).data("gfcountdown", plugin);
            // console.log($(this).data("gfcountdown"));
            // }
        });
    }
}(jQuery));